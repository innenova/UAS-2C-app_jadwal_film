package nova.inne.app_jadwal_film

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_setting.*

class SettingActivity: AppCompatActivity(), View.OnClickListener{
    var checkHD = ""
    lateinit var preferences: SharedPreferences
    val DEF_BACK = "Black"
    val PREF_NAME = "setting"
    val FONT_TITLE = "F"
    val DEF_FONT_SIZE = 20
    val FONT_CS = "G"
    val DEF_FONTHEAD_SIZE =20

    override fun onClick(v: View?) {
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val prefEditor = preferences.edit()
        prefEditor.putInt(FONT_TITLE,skTitle.progress)
        prefEditor.putInt(FONT_CS,seekBar.progress)
        prefEditor.commit()
        System.exit(0)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        skTitle.progress = preferences.getInt(FONT_TITLE, DEF_FONT_SIZE)
        seekBar.progress = preferences.getInt(FONT_CS,DEF_FONTHEAD_SIZE)
        btnsmpan.setOnClickListener(this)
        Toast.makeText(this,"Perubahan telah disimpan.",Toast.LENGTH_SHORT).show()

    }

    }
