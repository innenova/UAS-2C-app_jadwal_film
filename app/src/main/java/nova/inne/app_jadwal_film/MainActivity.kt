package nova.inne.app_jadwal_film

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.SeekBar
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var preferences: SharedPreferences
    val PREF_NAME = "setting"
    val DEFF_FILM = "Boss Baby 2"
    val DEF_CS = "COOMING SOON !!!"
    val TITLE_FILM = ""
    val FONT_TITLE = "F"
    val DEF_FONTHEAD_SIZE = 20
    val TITLE_CS = ""
    val FONT_CS = "G"
    val DEF_FONT_SIZE = 20


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.optionmenu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        txTitle.setText(preferences.getString(TITLE_FILM,DEFF_FILM))
        txTitle.setTextSize(preferences.getInt(FONT_TITLE,DEF_FONT_SIZE).toFloat())
        txCs.setText(preferences.getString(DEF_CS,DEF_CS))
        txCs.setTextSize(preferences.getInt(FONT_CS,DEF_FONTHEAD_SIZE).toFloat())




        btnJadwal.setOnClickListener(this)
        btnCs.setOnClickListener(this)
        btnTriller.setOnClickListener(this)



    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnJadwal ->{
                var jadwal = Intent(this,ActivityJadwal::class.java)
                startActivity(jadwal)
                true
            }
            R.id.btnCs ->{
                var film = Intent(this,AcitvityFilmCS::class.java)
                startActivity(film)
                true
            }
            R.id.btnTriller ->{
                var mv = Intent(this,Activity_Mv::class.java)
                startActivity(mv)
                true
            }
        }
    }





    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item?.itemId) {
            R.id.itemSetting -> {
                var intent = Intent(this,SettingActivity::class.java)
                startActivity(intent)

            }
        }

        return super.onOptionsItemSelected(item)
    }


}
