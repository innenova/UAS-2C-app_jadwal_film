package nova.inne.app_jadwal_film

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.acivity_film.*

class AdapterFilmCs (
    val dataFilCs : List<HashMap<String, String>>,
    val activityFilmCS: AcitvityFilmCS) :
    RecyclerView.Adapter<AdapterFilmCs.HolderFilmCs>() {

    class HolderFilmCs(v : View) : RecyclerView.ViewHolder(v){
        val txId_Film = v.findViewById<TextView>(R.id.txId_Film)
        val txJudulCs = v.findViewById<TextView>(R.id.txJudulCs)
        val txGenre = v.findViewById<TextView>(R.id.txGenre)
        val txSinop = v.findViewById<TextView>(R.id.txSinop)
        val photo = v.findViewById<ImageView>(R.id.imageView)
        val CLayout = v.findViewById<ConstraintLayout>(R.id.CLayout)
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterFilmCs.HolderFilmCs {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_filcs,p0, false)
        return HolderFilmCs(v)
    }

    override fun getItemCount(): Int {
        return dataFilCs.size
    }

    override fun onBindViewHolder(p0: AdapterFilmCs.HolderFilmCs, p1: Int) {
        val data = dataFilCs.get(p1)
        p0.txId_Film.setText(data.get("id_film"))
        p0.txJudulCs.setText(data.get("judul"))
        p0.txGenre.setText(data.get("genre"))
        p0.txSinop.setText(data.get("sinopsis"))

        if(p1.rem(2) == 0) p0.CLayout.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.CLayout.setBackgroundColor(Color.rgb(255,255,245))

        p0.CLayout.setOnClickListener({
            val pos = activityFilmCS.daftarGenre.indexOf(data.get("genre"))
            activityFilmCS.spgenree.setSelection(pos)
            activityFilmCS.edId_film.setText(data.get("id_film"))
            activityFilmCS.ed_judul.setText(data.get("judul"))
            activityFilmCS.edSinop.setText(data.get("sinopsis"))
            Picasso.get().load(data.get("url")).into(activityFilmCS.imgUploade)

        })

        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo)
    }
}