package nova.inne.app_jadwal_film

import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.MediaController
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_mv.*

class Activity_Mv : AppCompatActivity(), View.OnClickListener {
    val daftarVidio = intArrayOf(R.raw.mv1, R.raw.mv2, R.raw.mv3)
    var posVidSkrg = 0

    lateinit var  mediaController: MediaController
    override fun onClick(v: View?) {

    }

    var nextVid = View.OnClickListener {  x:View ->
        if(posVidSkrg<(daftarVidio.size-1)) posVidSkrg++
        else posVidSkrg = 0
        videoSet (posVidSkrg )
    }
    var prevVid = View.OnClickListener {  x:View ->
        if(posVidSkrg<0) posVidSkrg--
        else posVidSkrg = daftarVidio.size-1
        videoSet (posVidSkrg)
    }
    fun videoSet(pos : Int){
        videoView2.setVideoURI(Uri.parse("android.resource://"+packageName+"/"+daftarVidio[pos]))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mv)
        mediaController = MediaController(this)
        mediaController.setPrevNextListeners(nextVid,prevVid)
        mediaController.setAnchorView(videoView2)
        videoView2.setMediaController(mediaController)
        videoSet(posVidSkrg)
    }

}