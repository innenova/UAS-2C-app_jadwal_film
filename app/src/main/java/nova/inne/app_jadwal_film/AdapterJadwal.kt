package nova.inne.app_jadwal_film

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_jadwal.*

class AdapterJadwal (
    val dataJadwal : List<HashMap<String, String>>,
    val activityJadwal: ActivityJadwal) :
    RecyclerView.Adapter<AdapterJadwal.HolderJadwal>() {

    class HolderJadwal(v : View) : RecyclerView.ViewHolder(v){
        val txid_jadwal= v.findViewById<TextView>(R.id.txid_jadwal)
        val txjdl_film= v.findViewById<TextView>(R.id.txjdl_film)
        val txgenr = v.findViewById<TextView>(R.id.txgenr)
        val txTgl = v.findViewById<TextView>(R.id.txTgl)
        val tx_jam = v.findViewById<TextView>(R.id.tx_jam)
        val photo= v.findViewById<ImageView>(R.id.imgView2)
        val CLayout1 = v.findViewById<ConstraintLayout>(R.id.CLayout1)
    }
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterJadwal.HolderJadwal {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_jadwal,p0, false)
        return HolderJadwal(v)
    }

    override fun getItemCount(): Int {
        return dataJadwal.size
    }

    override fun onBindViewHolder(p0: AdapterJadwal.HolderJadwal, p1: Int) {
        val data = dataJadwal.get(p1)
        p0.txid_jadwal.setText(data.get("id_jadwal"))
        p0.txjdl_film.setText(data.get("judul"))
        p0.txgenr.setText(data.get("genre"))
        p0.txTgl.setText(data.get("tanggal"))
        p0.tx_jam.setText(data.get("jam"))

        if(p1.rem(2) == 0) p0.CLayout1.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.CLayout1.setBackgroundColor(Color.rgb(255,255,245))

        p0.CLayout1.setOnClickListener({
            val pos = activityJadwal.daftarGenre.indexOf(data.get("genre"))
            activityJadwal.sp_gendr.setSelection(pos)
            activityJadwal.edid_jadwal.setText(data.get("id_jadwal"))
            activityJadwal.ed_jdl.setText(data.get("judul"))
            activityJadwal.ed_tgl.setText(data.get("tanggal"))
            activityJadwal.ed_jam.setText(data.get("jam"))
            Picasso.get().load(data.get("url")).into(activityJadwal.imgUpload)

        })

        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo)
    }
    }

