package nova.inne.app_jadwal_film

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.acivity_film.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class AcitvityFilmCS:AppCompatActivity(), View.OnClickListener  {
    lateinit var mediaHelper: MediaHelper
    lateinit var filmcsAdapter : AdapterFilmCs
    lateinit var genreAdapter: ArrayAdapter<String>
    var daftarFilmCs = mutableListOf<HashMap<String,String>>()
    var daftarGenre = mutableListOf<String>()
    val url = "http://192.168.43.159/app-jadwal-film_web/show_filmcs.php"
    var url2 = "http://192.168.43.159/app-jadwal-film_web/show_genre.php"
    var url3 = "http://192.168.43.159/app-jadwal-film_web/query_ins_up_del_filmcs.php"
    var imStr = ""
    var pilihGenre = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.acivity_film)
        filmcsAdapter = AdapterFilmCs(daftarFilmCs,this)
        mediaHelper = MediaHelper(this)
        lsFilmCs.layoutManager = LinearLayoutManager(this)
        lsFilmCs.adapter = filmcsAdapter

        genreAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,
        daftarGenre)
        spgenree.adapter = genreAdapter
        spgenree.onItemSelectedListener = itemSelected

        imgUploade.setOnClickListener(this)
        btInsert.setOnClickListener(this)
        btUpdate.setOnClickListener(this)
        btDelete.setOnClickListener(this)
        btnFind.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imgUploade -> {
                val intent = Intent()
                intent.setType("image/*")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(intent,mediaHelper.getRcGallery())
            }
            R.id.btInsert -> {
                queryInsertUpdateDelete("insert")
            }
            R.id.btDelete -> {
                queryInsertUpdateDelete("delete")
            }
            R.id.btUpdate -> {
                queryInsertUpdateDelete("update")
            }
            R.id.btnFind -> {
                showDataFilmcs(ed_judul.text.toString().trim())
            }
        }
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spgenree.setSelection(0)
            pilihGenre = daftarGenre.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihGenre = daftarGenre.get(position)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if (requestCode == mediaHelper.getRcGallery()) {
                imStr = mediaHelper.getBitmapToString(data!!.data, imgUploade)
            }
        }
    }
    fun queryInsertUpdateDelete(mode : String) {
        val request = object : StringRequest(
            Method.POST, url3,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if (error.equals("000")) {
                    Toast.makeText(this, "Operasi Berhasil", Toast.LENGTH_LONG).show()
                    showDataFilmcs("")
                } else {
                    Toast.makeText(this, "Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }) {
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                val nmFile = "DC" + SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
                    .format(Date()) + ".jpg"
                when (mode) {
                    "insert" -> {
                        hm.put("mode", "insert")
                        hm.put("id_film", edId_film.text.toString())
                        hm.put("judul", ed_judul.text.toString())
                        hm.put("image", imStr)
                        hm.put("file", nmFile)
                        hm.put("genre", pilihGenre)
                        hm.put("sinopsis", edSinop.text.toString())
                    }
                    "update" -> {
                        hm.put("mode", "update")
                        hm.put("id_film", edId_film.text.toString())
                        hm.put("judul", ed_judul.text.toString())
                        hm.put("image", imStr)
                        hm.put("file", nmFile)
                        hm.put("genre", pilihGenre)
                        hm.put("sinopsis", edSinop.text.toString())
                    }
                    "delete" -> {
                        hm.put("mode", "delete")
                        hm.put("id_film", edId_film.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getNamaGenre(namaGenre : String) {
        val request = object : StringRequest(Request.Method.POST, url2,
            Response.Listener { response ->
                daftarGenre.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length() - 1)) {
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarGenre.add(jsonObject.getString("genre"))
                }
                genreAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG)
                    .show()
            }) {
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                hm.put("genre", namaGenre)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataFilmcs(namaFilmCs : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarFilmCs.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length() - 1)) {
                    val jsonObject = jsonArray.getJSONObject(x)
                    var film = HashMap<String, String>()
                    film.put("id_film", jsonObject.getString("id_film"))
                    film.put("judul", jsonObject.getString("judul"))
                    film.put("genre", jsonObject.getString("genre"))
                    film.put("sinopsis", jsonObject.getString("sinopsis"))
                    film.put("url", jsonObject.getString("url"))
                    daftarFilmCs.add(film)
                }
                filmcsAdapter.notifyDataSetChanged()
         },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }) {
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                hm.put("judul", namaFilmCs)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onStart() {
        super.onStart()
        showDataFilmcs("")
        getNamaGenre("")
    }
}

